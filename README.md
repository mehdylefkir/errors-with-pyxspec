# Errors with PyXspec

This script is meant to compute errors on fitted parameters for a given XSPEC model and the associated data. 
It also produces statistics plots to assess the minimization of the fit for all parameters.

## Requirements 

- Python 3 
- PyXspec running with Python 3
- Python 3 modules :
    * numpy 
    * scipy
    * matplotlib
    * PyPDF2
    * pandas

## Computing errors 

To compute errors run the function *ml_get_errors* with the right arguments as presented in the docstring of the function.

Example for a file model.xcm using cstat on a 8-core machine to get 90% confidence intervals :

```Python
ml_get_errors('model.xcm','cstat',n_cores=8,level=2.706)
```
